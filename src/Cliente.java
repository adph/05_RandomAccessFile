import javax.swing.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class Cliente {

    Scanner scan = new Scanner(System.in);

    //<editor-fold desc="Variables globales">
    private int codigo;
    private String nombre;
    private long sueldo;
    //</editor-fold>

    //<editor-fold desc="Constructores">
    public Cliente() {
    }

    public Cliente(int codigo, String nombre, long sueldo) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.sueldo = sueldo;
    }
    //</editor-fold>

    //<editor-fold desc="Getters & Setters">
    public int getCodigo() {

        return codigo;
    }

    //genera el codigo cliente secuencial
    public void setCodigo(String archivo) {

        int numeroClientes = 0;

        try (RandomAccessFile raf = new RandomAccessFile(archivo, "rw")) {

            //guardamos la longitud del archivo
            long tamanoRaf = raf.length();

            //longitud que ocupa en bytes los datos un cliente
            long tamanoDatosCliente = Integer.BYTES + Character.BYTES * 50 + Long.BYTES;

            //obtenemos los clientes que hay en el fichero
            numeroClientes = (int) (tamanoRaf / tamanoDatosCliente);

            //incrementamos el numero de clientes, para asignarselo al nuevo cliente
            numeroClientes++;


        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        //pasamos el numero de cliente
        this.codigo = numeroClientes;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre() {
        int rellenoEspacios, i;

        System.out.println("Nombre:");
        nombre = scan.nextLine();
        if (nombre.length() < 50) {
            rellenoEspacios = 50 - nombre.length();
            for (i = 0; i < rellenoEspacios; i++) {
                nombre = nombre + " ";
            }
        }
    }

    public long getSueldo() {
        return sueldo;
    }

    public void setSueldo() {
        Scanner scan = new Scanner(System.in);
        long sueldo;
        String numCadena;

        do {
            System.out.println("Sueldo: ");
            sueldo = scan.nextLong();
            numCadena = Long.toString(sueldo);

            if (numCadena.length() <= 1 || numCadena.length() > 10)
                JOptionPane.showMessageDialog(null, "El sueldo tiene que tener entre 1 y 10 digitos",
                        "Error", JOptionPane.ERROR_MESSAGE);

        } while (numCadena.length() <= 1 || numCadena.length() > 10);

        this.sueldo = sueldo;
    }
    //</editor-fold>

    //<editor-fold desc="Funciones">

    //guardamos el registro del cliente en un archivo de acceso aleatorio
    /*medidas de cada tipo de dato:
    int 4                           ---> codidoCliente 4
    String 2 * cada caracter        ---> nombre 100
    long 8                          ---> suledo 8
    total -----------------------------> 112 bytes cada registro
    */
    public void guardaDatosClienteEnFicheroBinario(String archivo, int codigo, String nombre, long saldo) throws IOException {
        /*medidas de cada tipo de dato:
        int 4                           ---> codidoCliente 4
        String 2 * cada caracter        ---> nombre 100
        long 8                          ---> suledo 8
        total -----------------------------> 112 bytes cada registro
        */

        //guardamos el registro del cliente en un archivo de acceso aleatorio
        //si ponemos (RandomAccessFile) delante de la { no hace falta cerrar el fichero con close()
        try (RandomAccessFile raf = new RandomAccessFile(archivo, "rw")) {

            //Ponemos el puntero al final de la longitud del fichero
            raf.seek(raf.length());

            //guardamos el codido el fichero
            raf.writeInt(codigo);

            /*Nos aeguramos que el campo String tendra 50 caracteres si no llega los rellenara con espacios y
            si son mas los ignorara
             */
            StringBuffer sb = new StringBuffer(nombre);
            sb.setLength(50);

            //guardamos el nombre en el fichero
            raf.writeChars(sb.toString());

            //guardamos el sueldo en el fichero
            raf.writeLong(saldo);


        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }

        JOptionPane.showMessageDialog(null, "Cliente introducido con exito.");
    }

    //busca registro en fichero de acceso aleatorio
    public void buscarCliente(String ficheroCliente) {

        boolean encontrado = false;

        //Introducimos el codCliente a buscar
        System.out.print("Introduce codigo de cliente: ");
        codigo = scan.nextInt();

        try (RandomAccessFile raf = new RandomAccessFile(ficheroCliente, "rw")) {

            //ponemos el puntero al principio del fichero
            raf.seek(0);

            //guardamos la longitud del archivo
            long tamanoRaf = raf.length();

            //longitud que ocupa en bytes los datos un cliente
            long tamanoDatosCliente = Integer.BYTES + Character.BYTES * 50 + Long.BYTES;

            /*recorremos el archivo y buscamos el cliente, a 'i' le vamos incrementando los bytes que ocupa
            los datos de un cliente
             */
            for (int i = 0; i < tamanoRaf; i += tamanoDatosCliente) {

                //posicionamos el puntero segun 'i'
                raf.seek(i);

                //si codigo == al codigo que apunta el puntero (seek) que se encuentra en el fichero
                if (codigo == raf.readInt()) {

                    /*hay que posicionar el puntero en al posicion anterior del dato, porque cada vez que lo utilizamos
                    (ver linea anterior 'if (codCliente == raf.readInt())' )
                    el puntero se coloca al final de ese dato.
                     */
                    raf.seek(i);

                    //Mostramos los datos del cliente
                    System.out.println(raf.readInt());
                    String nombre = "";
                    for (int j = 0; j < 50; j++) {
                        nombre += raf.readChar();
                    }
                    System.out.println(nombre);
                    System.out.println(raf.readLong());

                    encontrado = true;

                }
            }
            if (!encontrado) JOptionPane.showMessageDialog(null, "El codigo de cliente no existe", "Error",
                    JOptionPane.ERROR_MESSAGE);

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }
    //</editor-fold>

}
